#include "../include/image.h"
#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/transform.h"
#include "../include/util.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>




int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    //open files
    FILE *in_file;
    FILE *out_file;
    file_open_read(argv[1], &in_file);
    file_open_write(argv[2], &out_file);

    //work with images
    struct image image={0} ;
    struct image out_image={0} ;
    const enum read_status read_status = from_bmp(in_file, &image);
    is_successful_read(read_status);
    out_image = rotate_90(image);    

    //write to file
    const enum write_status write_status = to_bmp(out_file, &out_image);
    is_successful_writting(write_status);
   
    //close files
    close_file(in_file);
    close_file(out_file);
    
    //free memory
    image_destroy(image);
    image_destroy(out_image);



    return 0;
}
