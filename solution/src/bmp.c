#define _CRT_SECURE_NO_WARNINGS
#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/util.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define BF_TYPE 0x4D42
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define X_PELS_PER_METER 0
#define Y_PELS_PER_METER 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0


struct bmp_header create_header( struct image const image ) {
    return (struct bmp_header){
            .bfType = BF_TYPE,
            .bfileSize = (sizeof(struct bmp_header) + image.height* image.width * sizeof(struct pixel)
                          + image.height * ((image.width) % 4)),
            .bfReserved = BF_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biWidth = image.width,
            .biHeight = image.height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = BI_COMPRESSION,
            .biSizeImage = image.height * image.width * sizeof(struct pixel) + (image.width % 4)*image.height,
            .biXPelsPerMeter = X_PELS_PER_METER,
            .biYPelsPerMeter = Y_PELS_PER_METER,
            .biClrUsed = BI_CLR_USED ,
            .biClrImportant = BI_CLR_IMPORTANT};
}


bool read_header(FILE* f, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

bool ability_open_file(const char* filename, FILE** in_file)
{
    if (!filename) { return false;}
    *in_file = fopen(filename, "rb");
    if (!*in_file) {return false;}
    else {
        return true;
    }
    
}
bool ability_open_write_file(const char* filename, FILE** in_file){
    if (!filename) { return false;}
    *in_file = fopen(filename, "wb");
    if (!*in_file) {return false;}
    else {
        return true;
    }
    
}
/*  uint16_t bfType - 0x4d42 | 0x4349 | 0x5450
    uint32_t  bfileSize -  
    uint32_t bfReserved - 0
    uint32_t bOffBits -    , 54 = 16 + biSize
    uint32_t biSize -    
    uint32_t biWidth -   
    uint32_t  biHeight -   
    uint16_t  biPlanes - 1
    uint16_t biBitCount  24 
    uint32_t biCompression - BI_RGB
    uint32_t biSizeImage -     .   0
    uint32_t biXPelsPerMeter -  ,   
    uint32_t biYPelsPerMeter -   ,   
    uint32_t biClrUsed -   
    uint32_t  biClrImportant -   */
    
    size_t get_padding(uint32_t width) { return width % 4; }
    struct pixel* get_pixel(struct image* img, uint32_t i ){return &(img->data[i * img->width]);}
    enum read_status from_bmp(FILE* in, struct image* img ) 
    {
        struct bmp_header bmp_header;
        read_header(in, &bmp_header);

        if (bmp_header.bfType != 0x4d42 ) { return READ_INVALID_SIGNATURE; }
        if (bmp_header.biBitCount != 24) { return READ_INVALID_BITS; }
        if (bmp_header.bfReserved != 0) { return READ_INVALID_RESERVED; }
        if (bmp_header.biPlanes != 1) { return READ_INVALID_PLANES; }
        

        struct pixel* data = malloc(sizeof(struct pixel) * bmp_header.biWidth * bmp_header.biHeight);
        img->data = data;

        if ((bmp_header.biWidth) > 0 && (bmp_header.biHeight) > 0)
        {
            img->width = bmp_header.biWidth;
            img->height = bmp_header.biHeight;
        }
        else return READ_INVALID_WIDTH;
        uint32_t i=0;
        while (i < bmp_header.biHeight) 
        {
            if (!(fread(get_pixel(img, i), sizeof(struct pixel), img->width, in))) {
                return READ_INVALID_HEADER;
            }
            if(fseek(in, get_padding(bmp_header.biWidth), SEEK_CUR)){
                return READ_INVALID_HEADER;
            }
            // if don't work to change 1 to SEEK_CUR
            //means current position in the file
            i++;
        }
        return READ_OK;
    }


    enum write_status to_bmp(FILE* out, const struct image* img)
    {
        const size_t size = 0;
        struct bmp_header bmp_header = create_header(*img);
        if (!fwrite(&bmp_header, sizeof(struct bmp_header), 1, out))
        {
            return WRITE_ERROR_COUNT_CHARS;
        }
        uint32_t i = 0;
        const uint32_t height = img->height;
        while (i < height)
        {
            if (!(fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out))) {
                return WRITE_ERROR;
            }

            if (!(fwrite(&size, 1, get_padding(img->width) , out))) { return WRITE_ERROR; }
            i++;
        }
        return WRITE_OK;
    }

    
    void  is_successful_read(enum read_status read_status)
    {
        switch (read_status) {
        case READ_INVALID_SIGNATURE:
            util_err("Invalid signature\n");
            break;
        case READ_INVALID_BITS:
            util_err("Invalid bits\n");
            break;
        case READ_INVALID_HEADER:
            util_err("Invalid header\n");
            break;
        case READ_INVALID_RESERVED:
            util_err("Invalid reserved, always 0 \n");
            break;
        case READ_INVALID_PLANES:
            util_err("Invalid planes, always 1 \n");
            break;
        case READ_INVALID_WIDTH:
            util_err("Invalid width or height\n");
            break;
        case READ_OK:
            break;
        }

    }
    
    void  is_successful_writting(enum write_status  write_status)
    {
        switch (write_status) {
        case WRITE_ERROR:
            util_err("Write error \n");
            break;
        case WRITE_ERROR_COUNT_CHARS:
            util_err("Write error, problems with header\n");
            break;
        case WRITE_OK:
            break;
        }
    }

    void  close_file(FILE* file) {
        bool close = fclose(file);
        if (close) { util_err("close error\n"); }
    }
