#ifndef _BMP_H_
#define _BMP_H_

#include "image.h"
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)

/*  uint16_t bfType - 0x4d42 | 0x4349 | 0x5450
    uint32_t  bfileSize -  
    uint32_t bfReserved - 0
    uint32_t bOffBits -    , 54 = 16 + biSize
    uint32_t biSize -    
    uint32_t biWidth -   
    uint32_t  biHeight -   
    uint16_t  biPlanes - 1
    uint16_t biBitCount 0 | 1 | 4 | 8 | 16 | 24 | 32
    uint32_t biCompression - BI_RGB
    uint32_t biSizeImage -     .   0
    uint32_t biXPelsPerMeter -  ,   
    uint32_t biYPelsPerMeter -   ,   
    uint32_t biClrUsed -   
    uint32_t  biClrImportant -   */

struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)
struct bmp_header create_header( struct image const image);
//errors of reading
enum read_status {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_RESERVED,
    READ_INVALID_PLANES,
    READ_INVALID_WIDTH

};
//getting value
size_t get_padding(uint32_t width);
struct pixel* get_pixel(struct image* img,  uint32_t i );

//writes image from file
enum read_status from_bmp(FILE* in, struct image* img);

//errors of writting
enum  write_status {
    WRITE_OK,
    WRITE_ERROR,
    WRITE_ERROR_COUNT_CHARS
};
//writes image to file
enum write_status to_bmp( FILE* out, const struct image* img );

//check file open
bool ability_open_file(const char* filename, FILE** in_file);
bool ability_open_write_file(const char* filename, FILE** in_file);


bool read_header(FILE* f, struct bmp_header* header);
void  close_file(FILE* file);

//handles read errors
void  is_successful_read(enum read_status read_status);

//handles write errors
void  is_successful_writting(enum write_status  write_status);

#endif
