#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>


void file_open_write(const char* filename, FILE **out_file);
void file_open_read(const char* filename, FILE **in_file);
