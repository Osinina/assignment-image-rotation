#include "../include/image.h"
#include "../include/transform.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct image rotate_90(const struct image img)
{
    struct image output = {0};

    const uint32_t width = img.width;
    const uint32_t height = img.height;

    output.width = height;
    output.height = width;

    output.data = malloc(sizeof(struct pixel) * width * height);

    for (uint64_t i = 0; i < width; i++) {
        for (uint64_t j = 0; j < height; j++) {
            output.data[i * height + j] = img.data[(height - 1 - j) * width + i];
        }
    }

    return output;

    
}


void image_destroy(struct image image)
{
    free(image.data);
    
}
