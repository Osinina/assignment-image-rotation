#include <inttypes.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>

_Noreturn void util_err(const char* err);

void util_usage(int argc);

//for end string
