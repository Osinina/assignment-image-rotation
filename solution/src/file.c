#include "../include/bmp.h"
#include "../include/util.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

 void file_open_write(const char* filename, FILE **out_file){
    if (ability_open_write_file(filename, out_file)) 
     {
        printf("Opening output file was successful" );
    }
    else 
    {
        util_err("Failed to open file for writing \n");
    }
}

void file_open_read(const char* filename, FILE **in_file){
    
    if (ability_open_file(filename, in_file)) {
        printf("Opening input file was successful" );
    }
    else {
        util_err("Failed to open file for reading \n");
    }
}

   
