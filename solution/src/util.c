#include "../include/util.h"
#include <inttypes.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>


_Noreturn void util_err(const char* err) {
    fprintf(stderr, "%s", err);
    exit(1);
}

void util_usage(int argc) {
    if (argc != 3) {
        if (argc < 3) util_err("Not enough arguments \n");
        if (argc > 3) util_err("Too many arguments \n");
    }
    util_err("Invalid input \n");
}
