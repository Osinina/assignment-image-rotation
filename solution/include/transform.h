#ifndef _TRANSFORM_H_ 
#define _TRANSFORM_H_ 

#include "image.h"




//creates an updated image that is rotated 90 degrees
struct image rotate_90(const struct image img);


//free memory
void image_destroy(struct image image);

#endif
